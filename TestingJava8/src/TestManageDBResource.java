import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestManageDBResource {
	
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DBUtils.getConnection(DBType.ORADB);
			System.out.println("Connection to server established successfully");
//			conn = DBUtils.getConnection(DBType.MYSQLDB);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery("select * from MyCountries");
			rs.last();
			System.out.println("Total number of rows : " + rs.getRow());
		} catch (SQLException e) {
			DBUtils.showErrorMessage(e);
		}
		finally {
			conn.close();
		}
	}

}

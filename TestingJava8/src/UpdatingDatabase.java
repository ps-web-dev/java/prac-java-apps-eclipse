import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class UpdatingDatabase {
	public static void main(String[] args) throws SQLException {
//		updateUsingResultSet();
//		usingPreparedStatement();
		insertUsingPreparedStatement();
//		updateUsingPreparedStatement();
//		removeUsingPreparedStatement();
	}

	private static void removeUsingPreparedStatement() throws SQLException{
		Connection conn = DBUtils.getConnection(DBType.ORADB);
		String sql = "delete from newemployees where employee_id = ?";
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter emp id: ");
		int empno = Integer.parseInt(sc.nextLine());
		
		
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, empno);
		
		int res = pstmt.executeUpdate();
		if(res == 1 ) {
			System.out.println("Delete successful");
			
		}
		else {
			System.out.println("Error while deleting");
		}
		
	}

	private static void updateUsingPreparedStatement() throws SQLException{
		
		Connection conn = DBUtils.getConnection(DBType.ORADB);
		String sql = "Update newemployees set salary = ? where employee_id = ?";
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter emp id: ");
		int empno = Integer.parseInt(sc.nextLine());
		System.out.println("Enter salary: ");
		double salary = sc.nextDouble();
		
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setDouble(1, salary);
		pstmt.setInt(2, empno);
		
		int res = pstmt.executeUpdate();
		if(res == 1 ) {
			System.out.println("Update successful");
			
		}
		else {
			System.out.println("Error while updating");
		}
		
	}

	private static void insertUsingPreparedStatement() {
		
		int empno;
		String ename, email;
		Date hiredate;
		double salary;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter emp id: ");
		empno = Integer.parseInt(sc.nextLine());
		
		System.out.println("Enter emp name: ");
		ename = sc.nextLine();
		
		System.out.println("Enter emp email: ");
		email = sc.nextLine();
		
		System.out.println("Enter hire date: ");
		hiredate = Date.valueOf(sc.nextLine());
		
		System.out.println("Enter salary: ");
		salary = sc.nextDouble();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtils.getConnection(DBType.ORADB);
			String query = "insert into NewEmployees values (?,?,?,?,?)";
			pstmt =conn.prepareStatement(query);
			pstmt.setInt(1,empno);
			pstmt.setString(2, ename);
			pstmt.setString(3,email);
			pstmt.setDate(4, hiredate);
			pstmt.setDouble(5, salary);
			int result = pstmt.executeUpdate();
			if(result == 1) {
				System.out.println("Successful insert!");
			}
			else {
				System.out.println("Error while inserting!!!");
			}
			sc.close();
			pstmt.close();
			conn.close();
		}
		catch(SQLException e)
		{
			DBUtils.showErrorMessage(e);
		}
		
	}

	public static void updateUsingResultSet() throws SQLException {
		try (Connection conn = DBUtils.getConnection(DBType.ORADB);
				Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(
						"select Department_Id, Department_Name, Manager_Id, Location_Id from Departments")) {
			rs.absolute(6);
			rs.updateString("Department_name", "Information Techonlogy");
			rs.updateRow();
			System.out.println("Row updated successfully.");

			rs.moveToInsertRow();
			rs.updateInt("Department_Id", 999);
			rs.updateString("Department_Name", "Training");
			rs.updateInt("Manager_Id", 200);
			rs.updateInt("Location_Id", 1800);
			rs.insertRow();
			System.out.println("Record has been inserted...");
		} catch (SQLException e) {
			DBUtils.showErrorMessage(e);
		}
	}

	public static void usingPreparedStatement() throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtils.getConnection(DBType.ORADB);
			String query = "Select * From Employees where Salary < ? and Department_Id = ?";
			pstmt = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			prepareStatement(pstmt, 10000, 50);
			System.out.println("============================");
			prepareStatement(pstmt, 3000, 50);
			
		} catch (SQLException e) {
			DBUtils.showErrorMessage(e);
		}
	}

	private static void prepareStatement(PreparedStatement pstmt, double salary, int department_id) throws SQLException {
		ResultSet rs;
		pstmt.setDouble(1, salary);
		pstmt.setInt(2, department_id);
		rs = pstmt.executeQuery();

		String format = "%-4s%-20s%-25s%-10f\n";
		while (rs.next()) {
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));

		}
		rs.last();
		System.out.println("Total : " + rs.getRow());
	}
}

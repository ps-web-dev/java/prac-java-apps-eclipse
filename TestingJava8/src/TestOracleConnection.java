import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestOracleConnection {
	
	static final String dbUrl = "jdbc:oracle:thin:@localhost:1521:xe";
	
	static final String username = "hr";
	static final String password = "hr";
	
	public static void main(String[] args) {
		try {
			Connection conn = DriverManager.getConnection(dbUrl, username, password);
			System.out.println("Connection to sql database established successfully.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

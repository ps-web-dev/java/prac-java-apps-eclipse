import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestStaticSQLStatement {

	public static void main(String[] args) throws SQLException{
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;	
		
		try {
			conn = DBUtils.getConnection(DBType.MYSQLDB);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from Country");
			rs.last();
			System.out.println("Total rows : " + rs.getRow());
			
		} catch (SQLException e) {
			DBUtils.showErrorMessage(e);
		}
		finally {
			if(rs != null)
				rs.close();
			if(stmt != null)
				stmt.close();
			if(conn != null)
				conn.close();
		}

	}

}

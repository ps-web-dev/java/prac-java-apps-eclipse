import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Scanner;

public class BatrchProcessinginDB {
	public static void main(String[] args) {
		try(
				Connection conn = DBUtils.getConnection(DBType.ORADB);
				CallableStatement callableStmt = conn.prepareCall("{call AddNewEmployee(?,?,?,?,?)}");
		)
		{
			String option = "";
			do {
			
				Scanner sc = new Scanner(System.in);
			System.out.println("Enter emp id: ");
			int empno = Integer.parseInt(sc.nextLine());
			
			System.out.println("Enter emp name: ");
			String ename = sc.nextLine();
			
			System.out.println("Enter emp email: ");
			String email = sc.nextLine();
			
			System.out.println("Enter hire date: ");
			Date hiredate = Date.valueOf(sc.nextLine());
			
			System.out.println("Enter salary: ");
			double salary = sc.nextDouble();
			
			callableStmt.setInt(1, empno);
			callableStmt.setString(2, ename);
			callableStmt.setString(3, email);
			callableStmt.setDate(4, hiredate);
			callableStmt.setDouble(5, salary);
			callableStmt.addBatch();
			System.out.print("do yu want to add anothr recd y/n :");
			option = sc.nextLine();
			}
			while(option.equals("yes"));
			
			int[] updateCounts = callableStmt.executeBatch();
			System.out.println("Total records inserted are : " + updateCounts.length);
			
			
		}
		catch(SQLException e) {
			DBUtils.showErrorMessage(e);
		}
	}
}

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/home", "*.do"},
		name="Home",
		initParams= {@WebInitParam(name="product_name",
		value="Test")
		})
public class SimpleServlet extends HttpServlet{
	String appName = "EMPTY";
	String connstr = "string";
	String contextParam = "context";
	@Override
	public void init() throws ServletException {
		appName = getInitParameter("product_name");
		connstr = getInitParameter("connstr");
		
		contextParam = getServletContext().getInitParameter("context");
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		
		if(name != null) {
			resp.setContentType("text/xml");
			resp.getWriter().printf("<application>"
					+ "<name>Hello %s</name>"
					+ "<product>%s</product>"
					+ "<connection-string>%s</connection-string>"
					+ "<context>%s</context>"
					+ "</application>", name, appName, connstr, contextParam);
		}
		else {   
			resp.getWriter().write("Please enter a name");
		}
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		
		if(name != null && name != "") {
			resp.getWriter().printf("Hello %s", name);
		}
		else {
			resp.sendRedirect("index.jsp");
		}
		
	}
}
